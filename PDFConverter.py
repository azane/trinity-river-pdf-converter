# import sys  # default import, not actually being used by this program, could be used for debug
from reportlab.lib import colors  # reportlab imports are for tables and table formatting
from reportlab.lib.pagesizes import letter, inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
# from PyPDF2 import PdfFileWriter, PdfFileReader  # PyPDF2 allows reading and writing of pdfs
from reportlab.platypus.flowables import Image  # reportlab import which allows image handling
import json  # JavaScript Object Notation import
# from pprint import pprint  # console printing import for debug
from reportlab.platypus import Indenter  # allows indentation of elements
from reportlab.lib.colors import HexColor  # allows for custom coloring
import os

try:
    from message_merge import data as datafield
except ImportError:
    datafield = "data"

############################
#     CONSTANTS            #
############################

CONST_FILEPATHJSONFROM = 'tdsh-header.json'  # filepath to read JSON data from

CONST_FILEPATHPDFWRITE = 'simple_table_grid.pdf'  # filepath to write pdf to

CONST_ROWBACKGROUNDOFFSETCOLOR = HexColor('#f4f7fc')  # background color for the offset rows in the main and sum tables

# sizing consts for the main table, necessary because table does not accept expressions as widths
CONST_SMALLCOL = 0.3 * inch
CONST_MEDCOL = 0.35 * inch

# Use the following to size the 27 columns in the main table, be sure that the colwidths element contains 27 values,
# or you will receive a compilation error
# currently the first 22 are the same (small), last 5 are the same (med)
# colwidths = (CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL,
#              CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL,
#              CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL,
#              CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL, CONST_SMALLCOL,
#              CONST_MEDCOL, CONST_MEDCOL, CONST_MEDCOL, CONST_MEDCOL, CONST_MEDCOL,)
colwidths = (CONST_SMALLCOL,) * 22 + (CONST_MEDCOL,) * 5

#################################################################################
# WARNING - DO NOT CHANGE THESE UNLESS YOU KNOW WHAT YOU ARE DOING              #
# changing these values without changing the data going into the header file    #
# will cause a compilation error                                                #
CONST_HEADERTABLECOLS = 11  #
CONST_HEADERTABLEROWS = 8  #


#                                                                               #
#    These are included as a nicety for future development mostly               #
#################################################################################


class FieldGetter:
    def __init__(self, _jsondata):
        """

        :type _jsondata: dict
        """
        self._jsondata = _jsondata

    def get_level(self, *path):
        dd = self._jsondata
        for p in path:
            dd = dd[p]
        return dd

    def get_field(self, *path):
        dd = self.get_level(*path)
        return dd[datafield]

    def props(self, *path):
        dd = self.get_level(*path)
        return dd.iterkeys()


logcols = [
    "Log#", "LG", "D1", "D2", "SP", "SSL", "DL1", "DD1", "%D1", "FT1", "GD1", "DL2", "DD2", "%D2", "FT2",
    "GD2", "DL3", "DD3", "%D3", "FT3", "GD3", "MSM", "GRS", "NET", "SSLV", "UC", "SC"
]


def build_logs(field_getter):
    """

    :type field_getter: FieldGetter
    """
    logs = []

    for entity in field_getter.props():

        if entity.startswith("tdsh-log"):
            logcoldict = dict(zip(logcols, [""] * len(logcols)))

            logcoldict["LG"] = field_getter.get_field(entity, "length")
            logcoldict["D1"] = field_getter.get_field(entity, "diameter1")
            logcoldict["D2"] = field_getter.get_field(entity, "diameter2")
            logcoldict["SSL"] = field_getter.get_field(entity, "ss_length")
            logcoldict["SSLV"] = field_getter.get_field(entity, "ss_volume")
            # logcoldict["Log#"] = field_getter.get_field(entity, "log_no_on_load")

            # TODO totals over segs.
            gross = 0
            net = 0
            log = FieldGetter(field_getter.get_level(entity))
            for innerentity in log.props():
                if innerentity.startswith("tdsh-segment"):
                    segno = log.get_field(innerentity, "seg_no_on_log")

                    # FIXME to catch an issue on the device.
                    if segno == "":
                        segno = "1"

                    logcoldict["DL" + str(segno)] = log.get_field(innerentity, "length_deduction")
                    logcoldict["DD" + str(segno)] = log.get_field(innerentity, "diameter_deduction")
                    logcoldict["%D" + str(segno)] = log.get_field(innerentity, "percent_deduction")
                    logcoldict["FT" + str(segno)] = log.get_field(innerentity, "board_foot_deduction")
                    logcoldict["GD" + str(segno)] = log.get_field(innerentity, "grade_id")

                    _gross = log.get_field(innerentity, "gross")
                    _net = log.get_field(innerentity, "net")
                    gross += int(_gross if _gross != '' else 0)
                    net += int(_net if _net != '' else 0)

            logcoldict["GRS"] = gross
            logcoldict["NET"] = net

            logs.append([logcoldict[col] for col in logcols])

    return logs[::-1]


totcols = [
    "Species", "Quant.", "Segs.", "Gross", "Net",
    "SSL", "UC", "SC"
]


def get_index_of_first_string(lst, ss):
    return [i for i, col in enumerate(lst) if col == ss][0]


def e20(ss):
    return ss if ss != "" else 0


def build_tots(logs):
    spdict = {}

    spindex = get_index_of_first_string(logcols, "SP")
    grindex = get_index_of_first_string(logcols, "GRS")
    ssindex = get_index_of_first_string(logcols, "SSLV")

    for log in logs:

        # Add the species key if necessary.
        if log[spindex] not in spdict:
            # Pop the species out, we'll treat it as the key.
            t = spdict[log[spindex]] = dict(zip(totcols[1:], [0] * len(totcols)))
        else:
            t = spdict[log[spindex]]

        t["Quant."] += 1
        t["Gross"] += int(e20(log[grindex]))

        t["SSL"] += int(e20(log[ssindex]))

    return [[sp] + [deets[totcol] for totcol in totcols[1:]] for sp, deets in spdict.iteritems()]


def build_tot_tots(tots):
    tottot = [0] * (len(tots[-1]) - 1)

    for tot in tots:
        for i, t in enumerate(tot[1:]):
            tottot[i] += t

    return tottot


def get_pdf_elements(_jsondata):
    # container for the 'Flowable' objects, will hold the entire page, then append to the doc
    elements = []

    jj = FieldGetter(_jsondata)

    '''''''''''''''''''''''''''''
    Header Tables & Logo
    '''''''''''''''''''''''''''''

    # adding the logo to the top of the page
    cwd = os.getcwd()
    # headerlogo = Image(os.path.join(os.path.dirname(os.path.realpath(__file__)), "Form_Header.jpg"), 8.5 * inch, 1.5 * inch)  # sizing the logo
    # elements.append(headerlogo)  # appending logo to flowable

    # creating the date and ticket table data
    DTData = [["Date : " + str(jj.get_field("scale_date")), "Ticket# : " + str("---")]]
    DTTable = Table(DTData, 2 * [2 * inch], 1 * [0.3 * inch], hAlign='LEFT')  # creating the table itself

    # appending DTTable to the flowable
    elements.append(Indenter(left=0.3 * inch))  # adds indentation
    elements.append(DTTable)
    elements.append(Indenter(left=-0.3 * inch))  # removes indentation for future elements

    # creating the header table object with default labels,
    # the table contains lots of specific padding in the form of styling as well as empty elements
    # tabs show data elements as relative to column locations
    HeaderData = [
        ["Sale Name", " : " + str(), "", "", "Contract# ", "", " : " + str(), "",
         "", "Contract Minimums", ""],
        ["Logger", " : " + str(), "", "", "Source Load#", "", " : " + str(), "",
         "Diameter", " : ", str()],
        ["Seller", " : " + str(), "", "", "Seller Load#", "", " : " + str(), "",
         "Length", " : ", str()],
        ["Buyer", " : " + str(), "", "", "Buyer Load#", "", " : " + str(), "", "",
         "", ""],
        ["Trucker", " : " + str(jj.get_field("trucker_nn")), "", "", "Truck#", "",
         " : " + str(jj.get_field("truck_nn")), "", "",
         "Weight", ""],
        ["Destination", " : " + str(), "", "", "Scaler Code", "",
         " : " + str(), "", "", "Gross: " + str(jj.get_field("gross_weight")), ""],
        ["Deck", " : " + str(), "", "", "Sale Hammer", "", " : " + str(), "", "",
         "Tare: " + str(jj.get_field("tare_weight")), ""],
        ["Scale Type", " : " + str(), "", "", "", "", "", "", "", "Net: " + str(jj.get_field("net_weight")),
         ""]
    ]

    # defining the header table object as a reportlab table
    HeaderTable = Table(HeaderData, CONST_HEADERTABLECOLS * [0.7 * inch], CONST_HEADERTABLEROWS * [0.3 * inch],
                        spaceBefore=12, spaceAfter=12, hAlign='LEFT')

    # defining the style for the reportlab table object
    HeaderTable.setStyle(TableStyle([('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                                     ('VALIGN', (0, 0), (-1, -1), 'BOTTOM'),
                                     ('ALIGN', (9, 0), (9, -1), 'CENTER'),
                                     # align row 9 elements center, these are 'contract minimum', 'weights' and elements below 'weights'
                                     ('ALIGN', (10, 0), (10, -1), 'LEFT'),  # align "diameter" and "length" values
                                     ('ALIGN', (8, 1), (8, -1), 'RIGHT'),  # align "diameter" and "length" labels
                                     ('LINEBELOW', (8, 0), (10, 0), 0.25, colors.black),  # line for "contract minimum"
                                     ('LINEBELOW', (8, 4), (10, 4), 0.25, colors.black),  # line for "weight"
                                     ('SIZE', (0, 0), (-1, -1), 9.5)  # font size for header table
                                     ]))

    # appending the table to the final flowable obj
    elements.append(Indenter(left=0.3 * inch))  # adds indentation
    elements.append(HeaderTable)
    elements.append(Indenter(left=-0.3 * inch))  # removes indentation for future elements

    '''''''''''''''''''''''''''''
    Main Table
    '''''''''''''''''''''''''''''

    # getting data from the JSON obj
    logdata = build_logs(jj)
    TableData = logdata
    lognum = len(logdata)

    # Inserting the 2nd to top row of data, the labels, explicitly
    # logcols = [
    #     "Log#", "LG", "D1", "D2", "SP", "SSL", "DL", "DD", "%D", "FT", "GD", "DL", "DD", "%D", "FT",
    #     "GD", "DL", "DD", "%D", "FT", "GD", "MSM", "GRS", "NET", "SSL", "UC", "SC"
    # ]
    TableData.insert(0, logcols)

    lognum = lognum + 1  # increment the row counter to include this addition

    # Inserting the flying row with segment labels and empty elements
    TableData.insert(0, [
        "", "", "", "", "", "", "", "",
        "SEGMENT ONE",
        "", "", "", "",
        "SEGMENT TWO",
        "", "", "", "",
        "SEGMENT THREE",
        "", "", "", "", "", "",
        "ADJ GROSS", ""
    ])

    lognum = lognum + 1  # increment the row counter to include this addition

    # pprint(TableData) #debug


    # sizing the main data table, initiating with dynamic rows and constant columns
    MainTable = Table(TableData, colwidths, lognum * [0.25 * inch], spaceBefore=12, spaceAfter=12, hAlign='CENTER')

    # styling for the main table
    MainTable.setStyle(TableStyle([('ALIGN', (0, 0), (-1, -1), 'LEFT'),  # aligns all cells to the left
                                   ('ALIGN', (0, 0), (25, 1), 'CENTER'),  # aligns top row as center
                                   ('SIZE', (0, 0), (-1, -1), 6.5),  # sets the text size for the table
                                   ('VALIGN', (0, 0), (-1, -1), 'BOTTOM'),

                                   ('INNERGRID', (0, 1), (26, 1), 0.25, colors.black),
                                   ('INNERGRID', (0, 2), (-1, -1), 0.25, colors.white),
                                   ('BOX', (0, 1), (-1, -1), 0.2, colors.black),

                                   ('LINEABOVE', (6, 0), (20, 0), 0.25, colors.black),
                                   # top line for the three floating segments
                                   ('LINEBEFORE', (6, 0), (6, -1), 0.25, colors.black),  # left outer line ^
                                   ('LINEAFTER', (20, 0), (20, -1), 0.25, colors.black),  # right outer line ^
                                   ('LINEBEFORE', (11, 0), (11, -1), 0.25, colors.black),  # left inner line ^
                                   ('LINEBEFORE', (16, 0), (16, -1), 0.25, colors.black),  # right inner line ^

                                   ('LINEABOVE', (25, 0), (26, 0), 0.25, colors.black),
                                   # top line for adj gross floating segment
                                   ('LINEAFTER', (26, 0), (26, 0), 0.25, colors.black),  # right outer line ^
                                   ('LINEBEFORE', (25, 0), (25, 0), 0.25, colors.black),  # left outer line ^
                                   ('ALIGN', (25, 0), (26, 0), 'LEFT'),
                                   # aligning the "adj gross" text to fit in the box

                                   ('LINEBELOW', (0, 1), (-1, 1), 0.25, colors.black),  # label row (2nd row) underline

                                   ('ROWBACKGROUNDS', (0, 2), (-1, -1),
                                    [  # repeating, built in color changes on the rows
                                        CONST_ROWBACKGROUNDOFFSETCOLOR, colors.white  # light blue-grey and white
                                    ]),
                                   ]))

    # add Main Table to the flowable obj, no indentation
    elements.append(MainTable)

    '''''''''''''''''''''''''''''
    Summary Table
    '''''''''''''''''''''''''''''

    # getting the summary table info from the JSON obj
    TotData = build_tots(logdata[2:])  # exclude column headers.
    totnum = len(TotData)

    TotData.insert(0, totcols)

    totnum = totnum + 1  # increment the row counter to include this addition

    TotData.insert(0, [
        "", "", "", "", "", "", "ADJ GROSS", ""
    ])

    totnum = totnum + 1  # increment the row counter to include this addition

    # TotData.append([
    #     "Totals", "x", "x+1", "2x", "x^2", "x^x", "x/2", "x/x^2"
    # ])
    TotData.append(["Totals"] + build_tot_tots(TotData[2:]))
    totnum = totnum + 1  # increment the row counter to include this addition

    # creating sum table
    SummaryTable = Table(TotData, 8 * [0.4 * inch], totnum * [0.25 * inch], spaceBefore=12, spaceAfter=12,
                         hAlign='LEFT')

    # styling sum table
    SummaryTable.setStyle(TableStyle([('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                                      ('SIZE', (0, 0), (-1, -1), 6.5),  # font
                                      ('VALIGN', (0, 0), (-1, -1), 'BOTTOM'),

                                      ('ROWBACKGROUNDS', (0, 2), (-1, -1),
                                       [  # alternating colored backgrounds is luckily built in
                                           CONST_ROWBACKGROUNDOFFSETCOLOR, colors.white
                                       ]),

                                      ('INNERGRID', (0, 2), (-1, -1), 0.25, colors.white),

                                      ('LINEABOVE', (6, 0), (8, 0), 0.25, colors.black),
                                      # top line for adj gross floating segment
                                      ('LINEAFTER', (7, 0), (7, 0), 0.25, colors.black),  # right outer line ^
                                      ('LINEBEFORE', (6, 0), (6, 0), 0.25, colors.black),  # left outer line ^

                                      ('BOX', (0, 1), (-1, -1), 0.25, colors.black),

                                      ('LINEABOVE', (0, -1), (-1, -1), 0.25, colors.black),
                                      # line above bottom floating total
                                      ('BACKGROUND', (0, -1), (-1, -1), colors.white),
                                      # designating background as always white for bottom row
                                      ('INNERGRID', (0, -1), (-1, -1), 0.25, colors.black),  # inner grid for final row

                                      ]))

    # adding the summary table to the flowable object
    elements.append(Indenter(left=0.2 * inch))  # adds indentation
    elements.append(SummaryTable)
    elements.append(Indenter(left=-0.2 * inch))  # removes indentatino for future objects

    return elements


if __name__ == "__main__":
    '''''''''''''''''''''''''''''
    Getting JSON Data
    '''''''''''''''''''''''''''''
    # getting data as JSON object from a file
    with open(CONST_FILEPATHJSONFROM) as data_file:
        jsondata = json.load(data_file)

    # changing how the script recieves the JSON data may be done here, just be
    # sure to keep the name of the json data as "jsondata", and keep it
    # in an expected format

    # pprint(jsondata) # debug


    '''''''''''''''''''''''''''''
    Setting up PDF doc
    '''''''''''''''''''''''''''''
    # PDF doc setup
    doc = SimpleDocTemplate(CONST_FILEPATHPDFWRITE, pagesize=letter, rightMargin=0, leftMargin=0,
                            topMargin=0, bottomMargin=0)  # default doc object with no margins

    # write the document to file
    doc.build(get_pdf_elements(jsondata))
